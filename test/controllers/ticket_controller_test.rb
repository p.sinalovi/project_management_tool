require 'test_helper'

class TicketControllerTest < ActionDispatch::IntegrationTest
  test "should get subject:string" do
    get ticket_subject:string_url
    assert_response :success
  end

  test "should get description:text" do
    get ticket_description:text_url
    assert_response :success
  end

  test "should get priority:integer" do
    get ticket_priority:integer_url
    assert_response :success
  end

  test "should get project:reference" do
    get ticket_project:reference_url
    assert_response :success
  end

  test "should get state:string" do
    get ticket_state:string_url
    assert_response :success
  end

  test "should get closed_at:datetime" do
    get ticket_closed_at:datetime_url
    assert_response :success
  end

  test "should get estimated_hours:integer" do
    get ticket_estimated_hours:integer_url
    assert_response :success
  end

end
