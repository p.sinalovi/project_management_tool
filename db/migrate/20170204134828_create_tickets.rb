class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :subject
      t.text :description
      t.integer :priority
      t.references :project, foreign_key: true
      t.string :state
      t.datetime :closed_at
      t.integer :estimated_hours

      t.timestamps
    end
  end
end
