class CreateTimelogs < ActiveRecord::Migration[5.0]
  def change
    create_table :timelogs do |t|
      t.string :activity
      t.integer :spend_time
      t.references :ticket, foreign_key: true
      t.datetime :date
      t.text :comment

      t.timestamps
    end
  end
end
