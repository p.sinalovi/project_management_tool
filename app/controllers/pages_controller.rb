class PagesController < ApplicationController
  before_action :authenticate_user!, only:[:peoples]

  def home
  end

  def about 
  end 

  def peoples
  	@peoples = User.all
  end

  private 

end
