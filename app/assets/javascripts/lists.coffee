# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
$ ->
  $('#sorttable').tablesorter(
    theme: 'bootstrap'
    widthFixed: true
    widgets: [
      'columns'
    ]
    widgetOptions:
      columns: [
        'primary'
        'secondary'
        'tertiary'
      ]
      filter_cssFilter: [
        'form-control'
        'form-control'
        'form-control custom-select'
        'form-control'
        'form-control'
        'form-control'
        'form-control'
      ]).tablesorterPager
    
  return