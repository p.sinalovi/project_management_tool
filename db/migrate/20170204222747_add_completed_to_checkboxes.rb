class AddCompletedToCheckboxes < ActiveRecord::Migration[5.0]
  def change
    add_column :checkboxes, :completed_at, :datetime
  end
end
