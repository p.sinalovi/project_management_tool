class Ticket < ApplicationRecord
  belongs_to :project
  has_many :timelogs, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :checkboxes, dependent: :destroy
  
  accepts_nested_attributes_for :timelogs
  accepts_nested_attributes_for :comments
  accepts_nested_attributes_for :checkboxes
  def previous
  	Ticket.where(["id < ? and project_id =?", id, project_id]).last
  end

  def next
  	Ticket.where(["id > ? and project_id = ?", id, project_id]).first
  end

  def placement
    Ticket.where(["project_id = ?", project_id]).order('id ASC').index(self)
  end

end
