class ChangeColumnPriorityInTickets < ActiveRecord::Migration[5.0]
  def change
  	change_column :tickets, :priority, :string
  end
end
