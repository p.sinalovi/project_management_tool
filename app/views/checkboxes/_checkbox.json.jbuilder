json.extract! checkbox, :id, :content, :ticket_id, :created_at, :updated_at
json.url checkbox_url(checkbox, format: :json)