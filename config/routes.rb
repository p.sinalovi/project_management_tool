Rails.application.routes.draw do

  
  resources :projects do
    resources :tickets do
      member do
        put :change
      end
      resources :timelogs
      resources :checkboxes do
        member do 
          patch :complete
        end 
      end
      resources :comments
    end
  end
  resources :lists do 
    resources :tasks do
      member do 
        put :change
        put :complete
      end
    end
  end 
  
  get 'pages/home'
  get 'about' => 'pages#about'
  get 'peoples' => 'pages#peoples'

  root 'projects#index'
  devise_for :users, :controllers => { registrations: 'registrations' }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
