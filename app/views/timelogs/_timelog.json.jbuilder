json.extract! timelog, :id, :activity, :spend_time, :ticket_id, :date, :comment, :created_at, :updated_at
json.url timelog_url(timelog, format: :json)