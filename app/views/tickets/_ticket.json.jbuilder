json.extract! ticket, :id, :subject, :description, :priority, :project_id, :state, :closed_at, :estimated_hours, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)