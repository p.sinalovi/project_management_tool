class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :change, :edit, :update, :destroy]
  before_action :set_project
  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = @project.tickets.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @users = User.all
    @timelogs = @ticket.timelogs.all
    @comments = @ticket.comments.all
    @checkboxes = @ticket.checkboxes.all
    @comment = Comment.new
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
    @ticket.timelogs.build
    @ticket.comments.build
    @ticket.checkboxes.build
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = @project.tickets.create(ticket_params)

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @project, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @project, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def change
    @ticket.update_attributes(state: params[:state])
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Task state was successfully changed.' }
    end
  end
  def new_checkbox
  respond_to do |format|
    format.html
    format.js
  end
end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:subject, :description, :priority, :project_id, :state, :closed_at, :user_id, :estimated_hours)
    end

    def set_project
      @project = Project.find(params[:project_id])
    end
end
