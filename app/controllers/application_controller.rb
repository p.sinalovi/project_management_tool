class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  protect_from_forgery with: :exception
  before_action :load_lists

  private 
  def load_lists
    @lists = current_user.lists.all if current_user
  end
end
