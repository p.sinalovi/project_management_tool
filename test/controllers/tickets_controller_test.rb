require 'test_helper'

class TicketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get tickets_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_url
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post tickets_url, params: { ticket: { closed_at: @ticket.closed_at, description: @ticket.description, estimated_hours: @ticket.estimated_hours, priority: @ticket.priority, project_id: @ticket.project_id, state: @ticket.state, subject: @ticket.subject } }
    end

    assert_redirected_to ticket_url(Ticket.last)
  end

  test "should show ticket" do
    get ticket_url(@ticket)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_url(@ticket)
    assert_response :success
  end

  test "should update ticket" do
    patch ticket_url(@ticket), params: { ticket: { closed_at: @ticket.closed_at, description: @ticket.description, estimated_hours: @ticket.estimated_hours, priority: @ticket.priority, project_id: @ticket.project_id, state: @ticket.state, subject: @ticket.subject } }
    assert_redirected_to ticket_url(@ticket)
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete ticket_url(@ticket)
    end

    assert_redirected_to tickets_url
  end
end
