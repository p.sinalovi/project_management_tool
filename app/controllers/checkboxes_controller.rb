class CheckboxesController < ApplicationController
  before_action :set_checkbox, only: [:show, :complete, :edit, :update, :destroy]
  before_action :set_parents
  # GET /checkboxes
  # GET /checkboxes.json
  def index
    @checkboxes = Checkbox.all
  end

  # GET /checkboxes/1
  # GET /checkboxes/1.json
  def show
  end

  # GET /checkboxes/new
  def new
    @checkbox = Checkbox.new
  end

  # GET /checkboxes/1/edit
  def edit
  end

  # POST /checkboxes
  # POST /checkboxes.json
  def create
    @checkbox = @ticket.checkboxes.create(checkbox_params)

    respond_to do |format|
      if @checkbox.save
        format.html { redirect_to project_ticket_path(@project,@ticket), notice: 'Checkbox was successfully created.' }
        format.json { render :show, status: :created, location: @checkbox }
      else
        format.html { render :new }
        format.json { render json: @checkbox.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /checkboxes/1
  # PATCH/PUT /checkboxes/1.json
  def update
    respond_to do |format|
      if @checkbox.update(checkbox_params)
        format.html { redirect_to project_ticket_path(@project,@ticket), notice: 'Checkbox was successfully updated.' }
        format.json { render :show, status: :ok, location: @checkbox }
      else
        format.html { render :edit }
        format.json { render json: @checkbox.errors, status: :unprocessable_entity }
      end
    end
  end

  def complete
    @checkbox.update_attribute(:completed_at, Time.now)
    respond_to do |format|
      format.html{ redirect_to project_ticket_path(@project, @ticket)}
    end
  end 

  # DELETE /checkboxes/1
  # DELETE /checkboxes/1.json
  def destroy
    @checkbox.destroy
    respond_to do |format|
      format.html { redirect_to project_ticket_path(@project,@ticket), notice: 'Checkbox was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_checkbox
      @checkbox = Checkbox.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def checkbox_params
      params.require(:checkbox).permit(:content, :ticket_id, :project_id)
    end
        def set_parents 
      @ticket = Ticket.find(params[:ticket_id])
      @project = Project.find(params[:project_id])
    end
end
